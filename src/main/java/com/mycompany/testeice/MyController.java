/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testeice;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wandyer.silva
 */
@ManagedBean
@ViewScoped
public class MyController implements Serializable {
    
    private Resource resource;    
    
    @PostConstruct
    public void initMetaData() {        
        String path = "D:/upload/anexos/72/edital.pdf";

        File file = new File(path);
        try {
            this.resource = new MyResource(readIntoByteArray(new FileInputStream(file)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            this.resource = new MyResource(new byte[0]);
        }
    }
//    
//    public void download2() throws FileNotFoundException, IOException {
//        
//        String path = "D:/upload/anexos/72/3.pdf";
//        
//        InputStream in = new FileInputStream(path);
//        
//        byte[] buffer = new byte[4096];
//        int bytesRead;
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//
//        while ((bytesRead = in.read(buffer)) != -1) {
//            out.write(buffer, 0, bytesRead);
//        }
//        out.close();
//        downloadNext(out.toByteArray());
//    }
//    
//    public static void downloadNext(byte[] bs) throws IOException {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        ExternalContext externalContext = facesContext.getExternalContext();
//        HttpServletResponse response = (HttpServletResponse) externalContext
//                .getResponse();
//
//        response.reset();
//        response.setContentType("application/force-download");
//        response.setHeader("Content-Disposition", "attachment; filename=\file.pdf"); 
//        response.setContentLength(bs.length);
//
//        ServletOutputStream os = response.getOutputStream();
//        os.write(bs);
//
//        os.close();
//
//        facesContext.responseComplete();
//    }
    
    public void download() throws FileNotFoundException, IOException {  
                
        String destination  = "D:/upload/anexos/72/edital.pdf";
        String mimeType = "application/force-download";  
        
        File file = new File(destination);
        String fileName = file.getName();

        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpServletResponse response = 
                (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.reset();
        //response.setHeader("Content-Type", "application/pdf");
        response.setContentType( mimeType );  
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\""); 

        OutputStream responseOutputStream = response.getOutputStream();

        InputStream fileInputStream = new FileInputStream(file);

        byte[] bytesBuffer = new byte[2048];
        int bytesRead;
        while ((bytesRead = fileInputStream.read(bytesBuffer)) > 0) 
        {
            responseOutputStream.write(bytesBuffer, 0, bytesRead);
        }

        responseOutputStream.flush();

        fileInputStream.close();
        responseOutputStream.close();

        facesContext.responseComplete();
        

    }
    
    private static byte[] readIntoByteArray(InputStream in) throws IOException {
        byte[] buffer = new byte[4096];
        int bytesRead;
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        while ((bytesRead = in.read(buffer)) != -1) {
            out.write(buffer, 0, bytesRead);
        }
        out.flush();

        return out.toByteArray();
    }
    
    public class MyResource extends Resource implements java.io.Serializable {

        private String path = "";
        private HashMap<String, String> headers;
        private byte[] bytes;
        
        public MyResource(byte[] bytes) {
            this.bytes = bytes;
            this.headers = new HashMap<String, String>();
        }
        
        public InputStream getInputStream() {
            return new ByteArrayInputStream(this.bytes);
        }

        public String getRequestPath() {
            return path;
        }
        
        public void setRequestPath(String path) {
            this.path = path;
        }

        public Map<String, String> getResponseHeaders() {
            return headers;
        }

        public URL  getURL() {
            return null;
        }
        
        @Override
        public boolean userAgentNeedsUpdate(FacesContext context) {
            return false;
        }

    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }
}
